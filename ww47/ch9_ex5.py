from matplotlib import pyplot as plt

fhand = open("mails.txt")
Email = dict()
for line in fhand:
    line = line.rstrip()
    if not line.startswith("From "): continue
    words = line.split()
    mail = words[1]
    at = mail.find("@")
    emailname = mail[at + 1:]
    print(emailname)
    if emailname not in Email:
        Email[emailname] = 1
    else:
        Email[emailname] += 1
print(Email)

plt.plot(Email)
plt.xlabel("email name")
plt.ylabel("the message")
plt.title("diagram")

plt.legend()

plt.show()
