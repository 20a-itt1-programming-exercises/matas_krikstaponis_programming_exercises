abc = ["a", "b", "c"]


def chop(t):
    del t[0]
    del t[-1]


def middle(t):
    chop(t)
    return t


print(middle(abc))
