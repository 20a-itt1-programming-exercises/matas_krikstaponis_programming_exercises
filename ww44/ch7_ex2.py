#Ex2

fname = input('Enter the file name: ')
fhand = open(fname, 'r')

count = 0
total = 0
for line in fhand:
    if line.startswith("X-DSPAM-Confidence:"):
        space_index = line.find(" ")
        number = float(line[space_index+1:])
        total += number
        count += 1

print("The average number of file is:", total/count)
