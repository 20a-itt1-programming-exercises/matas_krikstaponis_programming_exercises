def code():
    try:
        firstnumber = input("enter the first number")
        if firstnumber == "done":
            print("Goodbye")
        else:
            secondnumber = input("enter the second number")
            if secondnumber == "done":
                print("Goodbye")
            else:
                n1 = int(firstnumber)
                n2 = int(secondnumber)
                calculator = input("Would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?")
                if calculator == "done":
                    print("Goodbye")
                else:
                    calculation = int(calculator)
                    if calculation == 1:
                        result = n1 + n2
                        print(n1, "added with", n2, " = ", result)
                        code()
                    elif calculation == 2:
                        result = n1 + n2
                        print(n1, "subtracted from", n2, " = ", result)
                        code()
                    elif calculation == 3:
                        if n2 == 0:
                            print("cannot divide by zero")
                            code()
                        else:
                            result = n1 / n2
                            print(n1, "divided from", n2, " = ", result)
                            code()
                    elif calculation == 4:
                        result = n1 * n2
                        print(n1, "multiplied by", n2, " = ", result)
                        code()
                    else:
                        print("choose one of the operations")
                        code()
    except:
        print("Only numbers and ""done"" are accepted as input, please try again")
        code()
        code()
